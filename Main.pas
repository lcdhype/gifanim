unit Main;

interface

uses
  Windows, Sysutils, Math, Graphics, GIFImg, Classes;

const
  MAX_AREAS = 255;

type
  TData = record
    Data: array[0..65535] of Byte;
  end;

  TVRAMBitmap = array[0..319, 0..319] of Byte;

  TAreas = array[0..MAX_AREAS, 0..3] of Integer;

  TConfigInfo = record
    gLCDx: Word;
    gLCDy: Word;
  end;

function GetID(): TData; stdcall;

procedure Init(); stdcall;

function GetDataEx(AParameter: PAnsiChar;
                   var ABitmap: TVRAMBitmap;
                   var AAreaList: TAreas;
                   var AAreaCount: Integer;
                   AConfig: TConfigInfo): TData; stdcall;


procedure CleanUpEx(var ABitmap: TVRAMBitmap;
                    var AAreaList: TAreas;
                    var AAreaCount: Integer;
                    AConfig: TConfigInfo); stdcall;

implementation

uses
  VersionInformation;

type
  TGIFParameer = record
    Filename: String;
    OffsetX: Integer;
    OffsetY: Integer;
    Inverted: Boolean;
    ColorMode: Boolean;
    Threshold: Integer;
  end;

  PBufferedAnimation = ^TBufferedAnimation;
  TBufferedAnimation = record
    Filename: String;
    ColorMode: Boolean;
    Threshold: Integer;
    Width: Integer;
    Height: Integer;
    CurrentFrameIndex: Integer;
    FrameCount: Integer;
    FrameList: array of array of array of Integer; // frame index, width, height
  end;

var
  gAnimationList: array of TBufferedAnimation;

function DecodeParameter(AParameter: String): TGIFParameer;
var
  p, l: Integer;

begin
  ZeroMemory(@result, SizeOf(result));
  result.Threshold := 128;

  l := Length(AParameter);
  p := Pos(',', AParameter);
  if p > 0 then begin
    result.Filename := Copy(AParameter, 1, p - 1);
    AParameter := Copy(AParameter, p + 1, l);

    p := Pos(',', AParameter);
    if p > 0 then begin
      result.OffsetX := StrToIntDef(Copy(AParameter, 1, p - 1), 0);
      AParameter := Copy(AParameter, p + 1, l);

      p := Pos(',', AParameter);
      if p > 0 then begin
        result.OffsetY := StrToIntDef(Copy(AParameter, 1, p - 1), 0);
        AParameter := Copy(AParameter, p + 1, l);

        p := Pos(',', AParameter);
        if p > 0 then begin
          result.Inverted := StrToIntDef(Copy(AParameter, 1, p - 1), 0) = 1;
          AParameter := Copy(AParameter, p + 1, l);

          p := Pos(',', AParameter);
          if p > 0 then begin
            result.Threshold := StrToIntDef(Copy(AParameter, 1, p - 1), 128);

            result.ColorMode := StrToIntDef(Copy(AParameter, p + 1, l), 0) = 1;
          end else
            result.Threshold := StrToIntDef(AParameter, 128);

        end else
          result.Inverted := StrToIntDef(AParameter, 0) = 1;

      end else
        result.OffsetY := StrToIntDef(AParameter, 0);

    end else
      result.OffsetX := StrToIntDef(AParameter, 0);

  end else
    result.Filename := AParameter;
end;

function EncodeColor(const ARed, AGreen, ABlue : Byte): Byte;
begin
  // lcdhype uses 3:3:2 encoding for colors, so encode 24bit color to 8 bit

  EncodeColor := (ARed AND $E0) + ((AGreen AND $E0) shr 3) + ((ABlue AND $C0) shr 6);
end;

function LoadAnimation(const AParameter: TGIFParameer; const AAnimationIndex: Integer = -1): Integer;
var
  i, lFrameIndex, y, x, lLuminance, lMaximum, lMinimum: integer;
  r,g,b: Byte;
  lGIFImage: TGIFImage;
  lColor: TColor;
  lAnimationIndex: Integer;
  lAnimation: PBufferedAnimation;
  lFrame: TGIFFrame;

begin
  if AAnimationIndex = -1 then begin
    lAnimationIndex := Length(gAnimationList);

    SetLength(gAnimationList, lAnimationIndex + 1);
  end else
    lAnimationIndex := AAnimationIndex;

  lAnimation := @gAnimationList[lAnimationIndex];

  lAnimation^.Filename := AParameter.Filename;
  lAnimation^.ColorMode := AParameter.ColorMode;
  lAnimation^.Threshold := AParameter.Threshold;

  lGIFImage := tgifimage.create;
  try
    lGIFImage.LoadFromFile(lAnimation^.Filename);

    lAnimation^.CurrentFrameIndex := 0;
    lAnimation^.Width := lGIFImage.Width;
    lAnimation^.Height := lGIFImage.Height;
    lAnimation^.FrameCount := lGIFImage.Images.Count;

    Setlength(lAnimation^.FrameList, lAnimation^.FrameCount, lAnimation^.Width, lAnimation^.Height);

    lFrameIndex := 0;
    while lFrameIndex < lAnimation^.FrameCount do begin
      lFrame := lGIFImage.Images[lFrameIndex];

      y := 0;
      while y < lAnimation^.Height do begin

        x := 0;
        while x < lAnimation^.Width do begin

          if (x >= lFrame.Left) and
             (y >= lFrame.Top) and
             (x < lFrame.BoundsRect.Right) and
             (y < lFrame.BoundsRect.Bottom)
          then
            i := lFrame.Pixels[x - lFrame.Left, y - lFrame.Top]
          else
            // if x and y is not within bounds of frame we also have to draw the
            // areas outside the bounds because GetDataEx() does not support transparency yet,
            // so just take the pixel color of first frame, this should make a partially
            // transparent frame to a fully opaque one
            i := lGIFImage.Images.Frames[0].Pixels[x, y];

          lColor := lFrame.ActiveColorMap.Colors[i];

          r := GetRValue(lColor);
          g := GetGValue(lColor);
          b := GetBValue(lColor);

          if AParameter.ColorMode then
            lAnimation^.FrameList[lFrameIndex, x, y] := EncodeColor(r, g, b)
          else begin
            lMaximum := MaxIntValue([r, g, b]);
            lMinimum := MinIntValue([r, g, b]);

            if (lMaximum + lMinimum) <> 0 then
              lLuminance := Ceil((lMaximum + lMinimum) / 2)
            else
              lLuminance := 0;

            if lLuminance > lAnimation^.Threshold then
              lAnimation^.FrameList[lFrameIndex, x, y] := 1
            else
              lAnimation^.FrameList[lFrameIndex, x, y] := 0;
          end;

          x := x + 1;
        end;

        y := y + 1;
      end;

      lFrameIndex := lFrameIndex + 1;
    end;

  finally
    lGIFImage.Free;
  end;

  result := lAnimationIndex;
end;

function GetAnimation(const AParameter: TGIFParameer): Integer;
var
  c, l: Integer;

begin
  result := -1;

  l := Length(gAnimationList);
  c := 0;
  while c < l do begin
    if gAnimationList[c].Filename = AParameter.Filename then
    begin
      result := c;

      c := l;
    end;

    c := c + 1;
  end;
end;

function GetID(): TData;
var
  lVersionInformation: TVersionInformation;
  lID: AnsiString;

begin
  lVersionInformation := TVersionInformation.Create();
  try
    lVersionInformation.Retrieve(GetModuleName(hInstance));
    lID := AnsiString(lVersionInformation.FileDescription) + ' v' + AnsiString(lVersionInformation.Version);
  finally
    lVersionInformation.Free;
  end;

  ZeroMemory(@result, SizeOf(result));
  StrCopy(PAnsiChar(@result.Data[0]), PAnsiChar(lID));
end;

procedure Init();
begin
  SetLength(gAnimationList, 0);
end;

function GetDataEx(AParameter: PAnsiChar;
                   var ABitmap: TVRAMBitmap;
                   var AAreaList: TAreas;
                   var AAreaCount: Integer;
                   AConfig: TConfigInfo): TData;
var
  x, y, w, h: Integer;
  lAnimationIndex: Integer;
  lParameter: TGIFParameer;
  lAnimation: PBufferedAnimation;

begin

  try
    lParameter := DecodeParameter(String(AnsiString(AParameter)));

    // get animation
    lAnimationIndex := GetAnimation(lParameter);
    if lAnimationIndex = -1 then
      lAnimationIndex := LoadAnimation(lParameter)
    else
    if (gAnimationList[lAnimationIndex].ColorMode <> lParameter.ColorMode) or
       (gAnimationList[lAnimationIndex].Threshold <> lParameter.Threshold)
    then
      lAnimationIndex := LoadAnimation(lParameter, lAnimationIndex);

    // output current animation frame
    lAnimation := @gAnimationList[lAnimationIndex];

    h := 0;
    while h < lAnimation^.Height do begin

      w := 0;
      while w < lAnimation^.Width do begin

        x := w + lParameter.OffsetX;
        y := h + lParameter.OffsetY;

        if (x >= 0) and (y >= 0) and (x < AConfig.gLCDx) and (y < AConfig.gLCDy) then
          if lParameter.Inverted then begin

            if lParameter.ColorMode then
              ABitmap[y, x] := not lAnimation^.FrameList[lAnimation^.CurrentFrameIndex, w, h]
            else
              ABitmap[y, x] := 1 - lAnimation^.FrameList[lAnimation^.CurrentFrameIndex, w, h];

          end else
            ABitmap[y, x] := lAnimation^.FrameList[lAnimation^.CurrentFrameIndex, w, h];

        w := w + 1;
      end;

      h := h + 1;
    end;

    // continue with next frame
    if lAnimation^.CurrentFrameIndex < lAnimation^.FrameCount-1 then
      lAnimation^.CurrentFrameIndex := lAnimation^.CurrentFrameIndex + 1
    else
      lAnimation^.CurrentFrameIndex := 0;

    // set plugin refresh area
    AAreaList[AAreaCount, 0] := lParameter.OffsetX;
    AAreaList[AAreaCount, 1] := lParameter.OffsetY;
    AAreaList[AAreaCount, 2] := lAnimation^.Width;
    AAreaList[AAreaCount, 3] := lAnimation^.Height;

    AAreaCount := AAreaCount + 1;
    if AAreaCount > MAX_AREAS then
      AAreaCount := MAX_AREAS;

  except
    on e: Exception do
      MessageBox(0, pchar(e.Message), nil, 0);
  end;

  FillChar(result.Data,sizeof(result.Data), 0);
end;

procedure CleanUpEx(var ABitmap: TVRAMBitmap;
                    var AAreaList: TAreas;
                    var AAreaCount: Integer;
                    AConfig: TConfigInfo);
begin
  // nothing to do here yet
end;

end.

